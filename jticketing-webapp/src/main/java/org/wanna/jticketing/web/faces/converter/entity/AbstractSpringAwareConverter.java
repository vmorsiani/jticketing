package org.wanna.jticketing.web.faces.converter.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.jsf.FacesContextUtils;

import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public abstract class AbstractSpringAwareConverter implements Converter {
    public final Log LOG = LogFactory.getLog(this.getClass());

    public Object getBean(FacesContext context, String bean){
        return FacesContextUtils.getWebApplicationContext(context).getBean(bean);
    }
}

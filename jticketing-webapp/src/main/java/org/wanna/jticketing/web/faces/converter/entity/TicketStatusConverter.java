package org.wanna.jticketing.web.faces.converter.entity;

import org.wanna.jticketing.persistence.entity.TicketStatusEntity;

import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = TicketStatusEntity.class)
public class TicketStatusConverter extends AbstractEntityConverter{
    public TicketStatusConverter(){
        super(TicketStatusEntity.class);
    }
}

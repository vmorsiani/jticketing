package org.wanna.jticketing.web.beans.dashboard;

import org.primefaces.component.dashboard.Dashboard;
import org.primefaces.component.panel.Panel;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.wanna.jticketing.web.beans.AbstractUIBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.html.HtmlOutputText;

@ManagedBean
@RequestScoped
public class DashboardViewBean extends AbstractUIBean {
    private Dashboard dashboard;

    @PostConstruct
    public void init(){
        dashboard = new Dashboard();
        dashboard.setId("dashboard");

        DashboardModel model = new DefaultDashboardModel();
        for( int i = 0, n = 2; i < n; i++ ) {
            DashboardColumn column = new DefaultDashboardColumn();
            model.addColumn(column);
        }
        dashboard.setModel(model);

        int items = 5;

        for( int i = 0, n = items; i < n; i++ ) {
            Panel panel = new Panel();
            panel.setId("measure_" + i);
            panel.setHeader("Dashboard Component " + i);
            panel.setClosable(true);
            panel.setToggleable(true);

            getDashboard().getChildren().add(panel);
            DashboardColumn column = model.getColumn(i%2);
            column.addWidget(panel.getId());
            HtmlOutputText text = new HtmlOutputText();
            text.setValue("Dashboard widget bits!" );

            panel.getChildren().add(text);
        }
    }
    public Dashboard getDashboard() {
        return dashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }
}

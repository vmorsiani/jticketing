package org.wanna.jticketing.web.faces.converter.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public abstract class AbstractEntityConverter extends AbstractSpringAwareConverter {
    private Class<? extends AbstractPersistable> clazz;
    public final Log LOG = LogFactory.getLog(this.getClass());

    public AbstractEntityConverter(Class<? extends AbstractPersistable> clazz){
        this.clazz = clazz;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        LOG.trace("converting "+clazz.getSimpleName()+" getAsObject for id "+(value==null?"null":value));
        if(value == null) return null;
        long id = Long.parseLong(value);
        AbstractPersistable object =  getEntityManager(context).find(clazz,id);
        LOG.trace("converting "+clazz.getSimpleName()+" found "+(object == null?"none":object.toString()+"with id "+object.getId()));
        return object;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        LOG.trace("converting "+clazz.getSimpleName()+" getAsObject for object "+(value==null?"null":value.toString()));
        if(value == null) return null;

        return String.valueOf(((AbstractPersistable)value).getId());
    }

    private EntityManager getEntityManager(FacesContext context){
        EntityManagerFactory factory = (EntityManagerFactory)super.getBean(context,"entityManagerFactory");
        return factory.createEntityManager();
    }
}

package org.wanna.jticketing.web.beans.ticket;

import org.wanna.jticketing.persistence.entity.TicketCategoryEntity;
import org.wanna.jticketing.persistence.entity.TicketEntity;
import org.wanna.jticketing.persistence.entity.TicketMessageEntity;
import org.wanna.jticketing.persistence.entity.UserEntity;
import org.wanna.jticketing.persistence.service.ticket.TicketService;
import org.wanna.jticketing.web.beans.AbstractUIBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean
@RequestScoped
public class TicketEditorBean extends AbstractUIBean{

    @ManagedProperty("#{ticketServiceImpl}")
    private TicketService ticketService;

    private TicketEntity ticket = new TicketEntity();

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public TicketEntity getTicket() {
        return ticket;
    }

    public void setTicket(TicketEntity ticket) {
        this.ticket = ticket;
    }

    public void create(TicketEntity ticket){
        LOG.info("creating ticket");
        ticket.setReporter(super.getCurrentUser());
        ticketService.createTicket(ticket);
    }

    public String assignToCurrentUser(TicketEntity ticket){
        LOG.info("assigning ticket "+ticket.getId()+" to user "+super.getCurrentUser().getUsername());
        ticketService.assign(ticket,super.getCurrentUser());
        return null;
    }

    public List<TicketCategoryEntity> getCategories(){
        return ticketService.getAvailableTicketCategory(super.getCurrentUser());
    }

    public List<TicketMessageEntity> getMessages(){
        return ticketService.getMessages(ticket, super.getCurrentUser());
    }

}

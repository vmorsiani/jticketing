package org.wanna.jticketing.web.beans.ticket;

import org.wanna.jticketing.persistence.entity.TicketEntity;
import org.wanna.jticketing.persistence.service.ticket.TicketService;
import org.wanna.jticketing.web.beans.AbstractUIBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean
@RequestScoped
public class TicketViewBean extends AbstractUIBean{

    @ManagedProperty("#{ticketServiceImpl}")
    private TicketService ticketService;

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public List<TicketEntity> getTickets() {
        return ticketService.findAll();
    }
}

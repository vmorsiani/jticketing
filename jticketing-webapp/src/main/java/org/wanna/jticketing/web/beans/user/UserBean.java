package org.wanna.jticketing.web.beans.user;

import org.wanna.jticketing.web.beans.AbstractUIBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.Locale;

@ManagedBean
@SessionScoped
public class UserBean extends AbstractUIBean implements Serializable {
    private Locale currentLocale = new Locale("en");

    public boolean isAuthenticated(){
        return super.getCurrentUser() != null;
    }

    public void setLocale(String locale){
        currentLocale = new Locale(locale);
    }

    public Locale getCurrentLocale() {
        return currentLocale;
    }
}

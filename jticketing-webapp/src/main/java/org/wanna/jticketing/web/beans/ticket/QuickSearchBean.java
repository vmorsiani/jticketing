package org.wanna.jticketing.web.beans.ticket;

import org.wanna.jticketing.persistence.service.ticket.TicketService;
import org.wanna.jticketing.web.beans.AbstractUIBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

@ManagedBean
@RequestScoped
public class QuickSearchBean extends AbstractUIBean{
    @ManagedProperty("#{ticketServiceImpl}")
    TicketService ticketService;

    private String ticketReference;

    public String getTicketReference() {
        return ticketReference;
    }

    public void setTicketReference(String ticketReference) {
        this.ticketReference = ticketReference;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public String search(){
        LOG.info("searching for reference "+ticketReference);
        return null;
    }
}

package org.wanna.jticketing.web.faces.converter.entity;

import org.wanna.jticketing.persistence.entity.TicketEntity;

import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = TicketEntity.class)
public class TicketConverter extends AbstractEntityConverter {
    public TicketConverter() {
        super(TicketEntity.class);
    }
}

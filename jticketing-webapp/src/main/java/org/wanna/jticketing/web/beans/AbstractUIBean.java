package org.wanna.jticketing.web.beans;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.wanna.jticketing.persistence.entity.UserEntity;


public abstract class AbstractUIBean {
    protected final Log LOG = LogFactory.getLog(this.getClass());
    /**
     * Retrieves the Currently logged in user from Spring security context
     *
     * @return current logged in user
     */
    public UserEntity getCurrentUser(){
        Authentication a = SecurityContextHolder.getContext().getAuthentication();
        Object o = a.getPrincipal();
        if(o instanceof UserEntity){
            return (UserEntity)o;
        }
        return null;
    }
}

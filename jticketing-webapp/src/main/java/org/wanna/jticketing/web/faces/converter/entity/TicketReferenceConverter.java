package org.wanna.jticketing.web.faces.converter.entity;

import org.wanna.jticketing.persistence.entity.TicketEntity;
import org.wanna.jticketing.persistence.service.ticket.TicketService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

@FacesConverter("ticketReferenceConverter")
public class TicketReferenceConverter extends AbstractSpringAwareConverter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        LOG.trace("converting ticket for reference "+value);
        return getTicketService(context).findByReference(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value != null && value instanceof TicketEntity) {
            TicketEntity ticket = (TicketEntity)value;
            return ticket.getReference();
        }else{
            return null;
        }
    }

    private TicketService getTicketService(FacesContext context){
        return (TicketService)super.getBean(context,"ticketServiceImpl");
    }
}

package org.wanna.jticketing.web.faces.converter.entity;

import org.wanna.jticketing.persistence.entity.TicketCategoryEntity;

import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = TicketCategoryEntity.class)
public class TicketCategoryConverter extends AbstractEntityConverter{

    public TicketCategoryConverter(){
        super(TicketCategoryEntity.class);
    }
}

package org.wanna.jticketing.persistence.service.ticket;

import org.wanna.jticketing.persistence.entity.TicketCategoryEntity;
import org.wanna.jticketing.persistence.entity.TicketEntity;
import org.wanna.jticketing.persistence.entity.TicketMessageEntity;
import org.wanna.jticketing.persistence.entity.UserEntity;

import java.util.List;
import java.util.Set;

public interface TicketService {
    List<TicketEntity> findAll();
    TicketEntity assign(TicketEntity ticket,UserEntity user);
    TicketEntity createTicket(TicketEntity ticket);
    TicketEntity findByReference(String reference);
    List<TicketCategoryEntity> getAvailableTicketCategory(UserEntity userEntity);
    List<TicketMessageEntity> getMessages(TicketEntity ticket, UserEntity user);
    void addMessage(TicketMessageEntity message, TicketEntity ticket);
}

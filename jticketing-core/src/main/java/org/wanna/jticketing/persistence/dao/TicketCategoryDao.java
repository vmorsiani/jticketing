package org.wanna.jticketing.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.wanna.jticketing.persistence.entity.TicketCategoryEntity;

public interface TicketCategoryDao extends JpaRepository<TicketCategoryEntity,Long>, JpaSpecificationExecutor<TicketCategoryEntity> {
}

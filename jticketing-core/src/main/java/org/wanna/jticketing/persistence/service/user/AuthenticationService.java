package org.wanna.jticketing.persistence.service.user;

import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 10/6/12
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AuthenticationService extends UserDetailsService{
}

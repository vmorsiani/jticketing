package org.wanna.jticketing.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.wanna.jticketing.persistence.entity.TicketMessageEntity;

public interface TicketMessageDao extends JpaRepository<TicketMessageEntity,Long>, JpaSpecificationExecutor<TicketMessageEntity> {
}

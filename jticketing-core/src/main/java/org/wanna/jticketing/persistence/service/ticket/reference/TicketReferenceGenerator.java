package org.wanna.jticketing.persistence.service.ticket.reference;


import org.wanna.jticketing.persistence.entity.TicketEntity;

public interface TicketReferenceGenerator {
    String generate(TicketEntity ticket);
}

package org.wanna.jticketing.persistence.dao.specification.ticket;


import org.springframework.data.jpa.domain.Specification;
import org.wanna.jticketing.persistence.entity.*;

import javax.persistence.criteria.*;
import java.util.Collection;

public class TicketSpecifications {
    public static Specification<TicketEntity> assignedToUser(final String username) {
        return new Specification<TicketEntity>() {
            public Predicate toPredicate(Root<TicketEntity> root, CriteriaQuery<?> query,
                                         CriteriaBuilder builder) {
                Join<TicketEntity,UserEntity> ticketAssignee = root.join(TicketEntity_.assignee);
                return builder.equal(ticketAssignee.get(UserEntity_.username),username);
            }
        };
    }

    public static Specification<TicketEntity> reportedByUser(final String username){
        return new Specification<TicketEntity>() {
            public Predicate toPredicate(Root<TicketEntity> root, CriteriaQuery<?> query,
                                         CriteriaBuilder builder) {
                Join<TicketEntity,UserEntity> ticketAssignee = root.join(TicketEntity_.reporter);
                return builder.equal(ticketAssignee.get(UserEntity_.username),username);
            }
        };
    }

    public static Specification<TicketEntity> inCategories(final Collection<String> categories){
        return new Specification<TicketEntity>() {
            public Predicate toPredicate(Root<TicketEntity> root, CriteriaQuery<?> query,
                                         CriteriaBuilder builder) {
                Join<TicketEntity,TicketCategoryEntity> ticketCategory = root.join(TicketEntity_.category);
                return ticketCategory.get(TicketCategoryEntity_.name).in(categories);
            }
        };
    }

    public static Specification<TicketEntity> withReference(final String reference){
        return new Specification<TicketEntity>() {
            @Override
            public Predicate toPredicate(Root<TicketEntity> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get(TicketEntity_.reference),reference);
            }
        };
    }

}

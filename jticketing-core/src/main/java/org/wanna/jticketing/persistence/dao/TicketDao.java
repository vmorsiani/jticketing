package org.wanna.jticketing.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.wanna.jticketing.persistence.entity.TicketEntity;

import java.util.List;

public interface TicketDao extends JpaRepository<TicketEntity,Long>, JpaSpecificationExecutor<TicketEntity> {
}

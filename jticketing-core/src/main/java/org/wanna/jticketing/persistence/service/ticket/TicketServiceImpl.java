package org.wanna.jticketing.persistence.service.ticket;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.wanna.jticketing.persistence.dao.TicketCategoryDao;
import org.wanna.jticketing.persistence.dao.TicketDao;
import org.wanna.jticketing.persistence.dao.TicketMessageDao;
import org.wanna.jticketing.persistence.dao.specification.ticket.TicketMessageSpecifications;
import org.wanna.jticketing.persistence.dao.specification.ticket.TicketSpecifications;
import org.wanna.jticketing.persistence.entity.*;
import org.wanna.jticketing.persistence.service.ticket.reference.DefaultReferenceGenerator;
import org.wanna.jticketing.persistence.service.ticket.reference.TicketReferenceGenerator;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

@Service
public class TicketServiceImpl implements TicketService{
    @Resource
    TicketDao ticketDao;
    @Resource
    TicketCategoryDao ticketCategoryDao;
    @Resource
    TicketMessageDao ticketMessageDao;

    TicketReferenceGenerator ticketReferenceGenerator = new DefaultReferenceGenerator();

    public void setTicketReferenceGenerator(TicketReferenceGenerator ticketReferenceGenerator) {
        this.ticketReferenceGenerator = ticketReferenceGenerator;
    }

    @Override
    public List<TicketEntity> findAll() {
        return ticketDao.findAll();
    }

    @Override
    public TicketEntity assign(TicketEntity ticket, final UserEntity user) {
        ticket.setAssignee(user);
        return ticketDao.save(ticket);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public TicketEntity createTicket(TicketEntity ticket) {

        TicketStatusEntity status = new TicketStatusEntity();
        status.setId(1L);
        ticket.setStatus(status);
        ticket = ticketDao.save(ticket);
        String reference = ticketReferenceGenerator.generate(ticket);

        ticket.setReference(reference);


        return ticketDao.save(ticket);
    }

    @Override
    public TicketEntity findByReference(final String reference) {
        return ticketDao.findOne(TicketSpecifications.withReference(reference));
    }

    @Override
    public List<TicketCategoryEntity> getAvailableTicketCategory(UserEntity userEntity){
        return ticketCategoryDao.findAll();
    }

    @Override
    public List<TicketMessageEntity> getMessages(TicketEntity ticket, UserEntity user) {
        return ticketMessageDao.findAll(TicketMessageSpecifications.fromTicket(ticket));
    }

    @Override
    public void addMessage(TicketMessageEntity message, TicketEntity ticket) {
        ticket.getMessages().add(message);
        ticketDao.save(ticket);
    }
}

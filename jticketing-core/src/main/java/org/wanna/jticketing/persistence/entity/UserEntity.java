package org.wanna.jticketing.persistence.entity;

import org.apache.commons.lang.builder.*;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class UserEntity extends AbstractPersistable<Long> implements UserDetails{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false,unique = true)
    private String username;
    @Basic
    private String password;
    @Column(nullable = false,unique = true)
    private String email;
    @ManyToOne
    private UserEntity parent;
    @ManyToMany
    private Set<UserGroupEntity> groups;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<UserGroupEntity> getGroups() {
        return groups;
    }

    public void setGroups(Set<UserGroupEntity> groups) {
        this.groups = groups;
    }

    public UserEntity getParent() {
        return parent;
    }

    public void setParent(UserEntity parent) {
        this.parent = parent;
    }

    public boolean hasParent(){
        return this.getParent() != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;

        if (!username.equals(that.username)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("username", username).
                toString();
    }

    /**
     * Spring security definition
     */

    /**
     * Retrieves the merged list of privileges granted to the user
     * It collect all the privileges from the different groups associated.
     *
     * @return all the privileges assigned to the user
     */
    @Override
    public Collection<PrivilegeEntity> getAuthorities() {
        Set<PrivilegeEntity> privileges = new HashSet<PrivilegeEntity>();
        for (UserGroupEntity group : groups) {
            privileges.addAll(group.getPrivileges());
        }
        return privileges;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

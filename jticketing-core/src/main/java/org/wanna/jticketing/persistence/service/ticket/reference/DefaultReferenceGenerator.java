package org.wanna.jticketing.persistence.service.ticket.reference;

import org.wanna.jticketing.persistence.entity.TicketEntity;

public class DefaultReferenceGenerator implements TicketReferenceGenerator{
    public static final String SEPARATOR = "-";

    @Override
    public String generate(final TicketEntity ticket) {
        String reference = "";

        reference += ticket.getCategory().getName();
        reference += SEPARATOR;
        reference += ticket.getId();

        return reference;
    }
}

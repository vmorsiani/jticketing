package org.wanna.jticketing.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.wanna.jticketing.persistence.entity.UserEntity;

/**
 * Created with IntelliJ IDEA.
 * User: vincent
 * Date: 10/6/12
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao extends JpaRepository<UserEntity,Long>{
    UserEntity findByUsername(String username);
}

package org.wanna.jticketing.persistence.dao.specification.ticket;

import org.springframework.data.jpa.domain.Specification;
import org.wanna.jticketing.persistence.entity.TicketEntity;
import org.wanna.jticketing.persistence.entity.TicketMessageEntity;
import org.wanna.jticketing.persistence.entity.TicketMessageEntity_;

import javax.persistence.criteria.*;

public class TicketMessageSpecifications {
    public static Specification<TicketMessageEntity> fromTicket(final TicketEntity ticket) {
        return new Specification<TicketMessageEntity>() {
            @Override
            public Predicate toPredicate(Root<TicketMessageEntity> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get(TicketMessageEntity_.ticket),ticket);
            }
        };
    }
}

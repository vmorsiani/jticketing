package org.wanna.jticketing.persistence.entity;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tickets")
public class TicketEntity extends AbstractPersistable<Long>{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Basic
    String reference;
    @ManyToOne
    @JoinColumn(nullable = false)
    private UserEntity reporter,account;
    @ManyToOne
    private UserEntity assignee;
    @OneToMany(mappedBy = "ticket")
    private Set<TicketMessageEntity> messages = new HashSet<TicketMessageEntity>();
    @Column(nullable = false)
    private String subject;
    @Lob
    private String description;
    @ManyToOne
    @JoinColumn(nullable = false)
    private TicketCategoryEntity category;
    @ManyToOne
    @JoinColumn(nullable = false)
    private TicketStatusEntity status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public TicketStatusEntity getStatus() {
        return status;
    }

    public void setStatus(TicketStatusEntity status) {
        this.status = status;
    }

    public UserEntity getReporter() {
        return reporter;
    }

    public void setReporter(UserEntity reporter) {
        this.reporter = reporter;
        if(reporter.hasParent()){
            this.setAccount(reporter.getParent());
        }else{
            this.setAccount(reporter);
        }
    }

    public UserEntity getAccount() {
        return account;
    }

    public void setAccount(UserEntity account) {
        this.account = account;
    }

    public UserEntity getAssignee() {
        return assignee;
    }

    public void setAssignee(UserEntity assignee) {
        this.assignee = assignee;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TicketCategoryEntity getCategory() {
        return category;
    }

    public void setCategory(TicketCategoryEntity category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketEntity that = (TicketEntity) o;

        if (reference != null ? !reference.equals(that.reference) : that.reference != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return reference != null ? reference.hashCode() : 0;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("reference", reference).
                toString();
    }

    public Set<TicketMessageEntity> getMessages() {
        return messages;
    }

    public void setMessages(Set<TicketMessageEntity> messages) {
        this.messages = messages;
    }
}

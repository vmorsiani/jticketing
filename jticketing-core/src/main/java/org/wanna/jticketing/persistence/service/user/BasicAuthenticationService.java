package org.wanna.jticketing.persistence.service.user;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.wanna.jticketing.persistence.dao.UserDao;
import org.wanna.jticketing.persistence.entity.UserEntity;

import javax.annotation.Resource;

@Service
public class BasicAuthenticationService implements AuthenticationService{
    @Resource
    UserDao userDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userDao.findByUsername(username);
        //Force privilege fetching
        userEntity.getAuthorities();
        return userEntity;
    }
}

package org.wanna.jticketing.persistence.service.user;

import junit.framework.Assert;
import org.junit.Test;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.wanna.jticketing.persistence.entity.UserEntity;
import org.wanna.jticketing.persistence.service.AbstractServiceTestCase;

import javax.annotation.Resource;

public class AuthenticationServiceTest extends AbstractServiceTestCase{
    @Resource
    AuthenticationService authenticationService;
    @Resource
    PasswordEncoder passwordEncoder;

    @Test
    public void testLoadByUsername(){
        final String username = "admin";
        final String password = "admin";
        final String encodedPassword = passwordEncoder.encodePassword(password,null);

        UserDetails userDetails = authenticationService.loadUserByUsername(username);
        //Ensure the proper user is returned
        Assert.assertTrue(userDetails instanceof UserEntity);
        Assert.assertEquals(username,userDetails.getUsername());
        Assert.assertEquals(encodedPassword,userDetails.getPassword());
        //Ensure Authority collection have been populated
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
    }
}

package org.wanna.jticketing.persistence.service.ticket;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.wanna.jticketing.persistence.entity.TicketCategoryEntity;
import org.wanna.jticketing.persistence.entity.TicketEntity;
import org.wanna.jticketing.persistence.entity.TicketMessageEntity;
import org.wanna.jticketing.persistence.entity.UserEntity;
import org.wanna.jticketing.persistence.service.AbstractServiceTestCase;

import javax.annotation.Resource;
import java.util.List;

public class TicketServiceTest extends AbstractServiceTestCase{
    @Resource
    private TicketService ticketService;

    private UserEntity reporter;
    private TicketEntity ticket;
    private TicketCategoryEntity category;

    @Before
    public void before(){
        reporter = new UserEntity();
        reporter.setId(1L);

        category = new TicketCategoryEntity();
        category.setName("SUPPORT");
        category.setId(1L);

        ticket = new TicketEntity();
        ticket.setSubject("this is a unit test ticket");
        ticket.setReporter(reporter);
        ticket.setCategory(category);
    }

    @Test
    public void testfindAll(){
        List<TicketEntity> tickets = ticketService.findAll();
        Assert.assertNotNull(tickets);
        Assert.assertFalse(tickets.isEmpty());
    }

    @Test
    public void testReferenceOnCreate(){
        ticket = ticketService.createTicket(ticket);
        Assert.assertNotNull(ticket.getReference());
        LOG.debug(ticket.getReference());
    }

    @Test
    public void addMessage(){
        ticket = ticketService.createTicket(ticket);
        TicketMessageEntity message = new TicketMessageEntity();
        message.setAuthor(reporter);
        message.setText("This is a message from unit test");
        ticketService.addMessage(message,ticket);
    }
}

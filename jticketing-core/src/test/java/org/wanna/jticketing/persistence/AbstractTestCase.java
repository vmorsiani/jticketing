package org.wanna.jticketing.persistence;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class AbstractTestCase {
    public final Log LOG = LogFactory.getLog(this.getClass());
}

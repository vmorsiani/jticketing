package org.wanna.jticketing.persistence.service;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wanna.jticketing.persistence.AbstractTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:applicationContext-resources.xml",
        "classpath:applicationContext-dao.xml",
        "classpath:applicationContext-service.xml"
})
public abstract class AbstractServiceTestCase extends AbstractTestCase{

}

package org.wanna.jticketing.persistence.dao;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wanna.jticketing.persistence.AbstractTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:applicationContext-resources.xml",
        "classpath:applicationContext-dao.xml"
})
public abstract class AbstractDaoTestCase extends AbstractTestCase{

}

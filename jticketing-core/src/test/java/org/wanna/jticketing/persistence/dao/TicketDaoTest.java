package org.wanna.jticketing.persistence.dao;

import junit.framework.Assert;
import org.junit.Test;
import org.wanna.jticketing.persistence.dao.specification.ticket.TicketSpecifications;
import org.wanna.jticketing.persistence.entity.TicketEntity;
import static org.springframework.data.jpa.domain.Specifications.*;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class TicketDaoTest extends AbstractDaoTestCase {
    @Resource
    TicketDao ticketDao;

    @Test
    public void testFindAll(){
        List<TicketEntity> tickets = ticketDao.findAll();
        Assert.assertNotNull(tickets);
        Assert.assertFalse(tickets.isEmpty());
    }

    @Test
    public void testFindByReporter(){
        final String reporter = "user";
        List<TicketEntity> tickets = ticketDao.findAll(TicketSpecifications.reportedByUser(reporter));
        Assert.assertNotNull(tickets);
        Assert.assertFalse(tickets.isEmpty());
        for (TicketEntity ticket : tickets) {
            Assert.assertEquals(reporter,ticket.getReporter().getUsername());
        }
    }

    @Test
    public void testFindByReporterAndAssignee(){
        final String reporter = "user";
        final String assignee = "operator";
        List<TicketEntity> tickets = ticketDao.findAll(
                where(TicketSpecifications.reportedByUser(reporter))
                        .and(TicketSpecifications.assignedToUser(assignee))
        );
        Assert.assertNotNull(tickets);
        Assert.assertFalse(tickets.isEmpty());
        Assert.assertEquals(1,tickets.size());
        for (TicketEntity ticket : tickets) {
            Assert.assertEquals(reporter,ticket.getReporter().getUsername());
            Assert.assertEquals(assignee,ticket.getAssignee().getUsername());
        }
    }

    @Test
    public void testFindByCategories(){
        final String support = "SUPPORT";
        final String sales = "SALES";

        List<String> categories = new ArrayList<String>();
        categories.add(sales);
        categories.add(support);

        List<TicketEntity> tickets = ticketDao.findAll(TicketSpecifications.inCategories(categories));
        Assert.assertNotNull(tickets);
        for (TicketEntity ticket : tickets) {
            Assert.assertTrue(categories.contains(ticket.getCategory().getName()));
        }
    }
}

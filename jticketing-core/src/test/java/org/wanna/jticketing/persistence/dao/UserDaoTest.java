package org.wanna.jticketing.persistence.dao;

import junit.framework.Assert;
import org.junit.Test;
import org.wanna.jticketing.persistence.entity.UserEntity;

import javax.annotation.Resource;

public class UserDaoTest extends AbstractDaoTestCase{
    @Resource
    UserDao userDao;

    @Test
    public void testLoadByUsername(){
        final String username = "admin";
        UserEntity user = userDao.findByUsername(username);
        Assert.assertEquals(username,user.getUsername());
    }
}

package org.wanna.jticketing.persistence.service.ticket.reference;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.wanna.jticketing.persistence.entity.TicketCategoryEntity;
import org.wanna.jticketing.persistence.entity.TicketEntity;

public class DefaultReferenceGeneratorTest {

    private TicketEntity ticket;
    private TicketCategoryEntity category;
    private TicketReferenceGenerator generator = new DefaultReferenceGenerator();

    @Before
    public void before(){
        category = new TicketCategoryEntity();
        category.setName("TEST");
        ticket = new TicketEntity();
        ticket.setCategory(category);
        ticket.setId(1L);
    }

    @Test
    public void testGenerateReference(){
        String reference = generator.generate(ticket);
        String expected = category.getName()+DefaultReferenceGenerator.SEPARATOR+ticket.getId();
        Assert.assertEquals(expected,reference);
    }
}
